<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Contracts\Translation\TranslatorInterface;

#[AsCommand(
    name: 'app:translate',
    description: 'Add a short description for your command',
)]
class CreateImageCommand extends Command
{
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        parent::__construct();
        $this->translator = $translator;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

//        if (!extension_loaded('intl')) {
//            $io->writeln('The intl extension is not available.');
//
//            return Command::FAILURE;
//        }

        $translatedMessage = $this->translator->trans(id: 'hello.world', locale: 'fr');
        $output->writeln("Translated Message: $translatedMessage");

        return Command::SUCCESS;
    }
}
